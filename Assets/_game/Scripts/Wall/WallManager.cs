using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Fusion;

public class WallManager : NetworkBehaviour
{
    [SerializeField] private TextMeshProUGUI _wallHealthText = null;

    public NetworkRunner runner = null;

    private string _entryText = "Wall: ";

    [Networked] public float wallHealth { get; set; } = 100f;

    public void DamageWall(int p_damageAmount)
    {
        wallHealth -= p_damageAmount;
        AttackerCameraRaycast.attackersTroopsAttacking = false;
        AttackerCameraRaycast.damageAmount = 0; // disabling the damage amount
        Debug.Log("Wall damaged");
        Debug.Log("Wall health: " + wallHealth);
    }
    [Rpc]
    private void Rpc_TextUpdate()
    {
        _wallHealthText.text = _entryText + wallHealth;
    }
    public override void FixedUpdateNetwork()
    {
        Rpc_TextUpdate(); // test        
        //wallHealth += runner.DeltaTime * 3;
    }
}
