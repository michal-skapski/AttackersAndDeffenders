using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WallBehaviour : MonoBehaviour
{
    [SerializeField] private WallManager _wallManager = null;

    public bool isAttacked = false;
    private void OnMouseDown() // add if for recognizing the player later
    {
        if (isAttacked is true)
        {
            SendInfo(AttackerCameraRaycast.damageAmount);
        }
    }
    public void SendInfo(int p_damageAmount)
    {
        Debug.Log("Sending info");
        _wallManager.DamageWall(p_damageAmount);
        isAttacked = false;
        AttackerCameraRaycast.attackersTroopsAttacking = false;
    }
}