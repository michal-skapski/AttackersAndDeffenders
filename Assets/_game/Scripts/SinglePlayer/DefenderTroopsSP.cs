using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderTroopsSP : MonoBehaviour
{
    [SerializeField] private int _healthPoints = 0;
    public int attackPower = 0;
    public bool isAttacked = false;

    public int damage = 0;


    private const int _minAttackDelay = 15;
    private const int _maxAttackDelay = 40;

    private const int _minIndex = 0;

    private int _choseTarget = 0;

    private void OnMouseDown()
    {
        if (isAttacked == true)
        {
            GetDamage();
        }
    }
    public void GetDamage()
    {
        _healthPoints -= damage;
        isAttacked = false;
        AttackerCameraRaycastSP.attackersTroopsAttacking = false; // disabling so the player needs to use commend attack once again
        LifeCheck();
    }
    private void LifeCheck()
    {
        if (_healthPoints <= 0)
        {
            Destroy(this.gameObject);
        }
    }
    // code for attack obj from list
    private IEnumerator Attack() // attack attacker 
    {
        int newDelay = Random.Range(_minAttackDelay, _maxAttackDelay);
        yield return new WaitForSeconds(newDelay);
        RandomizeTarget();
        if (SpawnManagerSP.currentTroops.Count == 0)
        {
            yield break; // break - breaking iteration - stops coroutine if player has no troops
        }
        AttackerTroopsSP attackedObj = SpawnManagerSP.currentTroops[_choseTarget].gameObject.GetComponent<AttackerTroopsSP>();
        attackedObj.GetAttacked(attackPower);
        StartCoroutine(Attack());
    }
    private void RandomizeTarget()
    {
        int maxIndex = SpawnManagerSP.currentTroops.Count;
        if(maxIndex == _minIndex)
        {
            _choseTarget = 0;
        }
        _choseTarget = Random.Range(_minIndex, maxIndex);
    }
    private void Start()
    {
        StartCoroutine(Attack());
    }
}