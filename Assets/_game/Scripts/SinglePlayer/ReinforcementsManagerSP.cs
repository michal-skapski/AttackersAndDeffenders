using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ReinforcementsManagerSP : MonoBehaviour
{
    [SerializeField] private WinManagerSP _winManager = null;

    [SerializeField] private TMP_Text _timerText = null;

    public bool gameInPlay = true;

    private int _oneSec = 1;

    private bool _takeTime = false;

    private string _reinforcementsText = "Castle's reinforcements arrive in: ";
    private string _secondsText = " seconds!";

    [SerializeField] private int _timeLeft = 0;

    private void TimeCheck()
    {
        if(_timeLeft > 0)
        {
            return;
        }
        else
        {
            _timerText.gameObject.SetActive(false); // disabling the obj
            _winManager.DefenderWin();
            // win manager send info
        }
    }
    private IEnumerator TimerTake()
    {
        _takeTime = true;
        yield return new WaitForSeconds(_oneSec);
        Debug.Log("Decrease time");
        _timeLeft--;
        TimeCheck();
        _takeTime = false;
    }
    private void TextUpdate()
    {
        _timerText.text = _reinforcementsText + _timeLeft + _secondsText;
        if (gameInPlay == false)
        {
            _timerText.gameObject.SetActive(false);
        }
    }
    private void Update()
    {
        if (_timeLeft > 0 && _takeTime == false && gameInPlay == true)
        {
            StartCoroutine(TimerTake());
        }
        TextUpdate();
    }
}