using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SpawnManagerSP : MonoBehaviour
{
    [SerializeField] private GameObject[] _recruitFields = null;


    [SerializeField] private GameObject _batteringRam = null;
    [SerializeField] private GameObject _ballista = null;
    [SerializeField] private GameObject _catapult = null;
    [SerializeField] private GameObject _canon = null;

    [SerializeField] private Button[] _troopsButtons = null;
    [SerializeField] private Button _batteringRamButton = null;
    [SerializeField] private Button _ballistaButton = null;
    [SerializeField] private Button _catapultButton = null;
    [SerializeField] private Button _canonButton = null;

    [SerializeField] private Button _attackButton = null;

    private Vector3 _batteringRamStartPos = new Vector3(-21.5f, 0f, -33.1f);

    private int _currentFieldID = 0;
    private int _troopID = 0;

    public static List<GameObject> currentTroops = new List<GameObject>();

    public void AttackersTroopsUISetActive(bool p_setActive)
    {
        for (int i = 0; i < _troopsButtons.Length; i++)
        {
            _troopsButtons[i].gameObject.SetActive(p_setActive);
        }
    }
    public void PrepareToSpawn(int p_fieldID)
    {
        _currentFieldID = p_fieldID;

        AttackersTroopsUISetActive(true);
    }
    private void BatteringRamSpawn()
    {
        GameObject newBatteringRam = Instantiate(_batteringRam, _batteringRamStartPos, Quaternion.identity);
        newBatteringRam.GetComponent<AttackerTroopsSP>().AttackButton = _attackButton;
        newBatteringRam.GetComponent<AttackerTroopsSP>().TroopID= _troopID;
        FieldDeactivate();
        AddToList(newBatteringRam);
    }
    private void CatapultSpawn()
    {
        GameObject newCatapult = Instantiate(_catapult, _recruitFields[_currentFieldID].gameObject.transform.position, Quaternion.identity);
        newCatapult.GetComponent<AttackerTroopsSP>().AttackButton = _attackButton;
        newCatapult.GetComponent<AttackerTroopsSP>().TroopID= _troopID;
        FieldDeactivate();
        AddToList(newCatapult);
    }
    private void BallistaSpawn()
    {
        GameObject newBallista = Instantiate(_ballista, _recruitFields[_currentFieldID].gameObject.transform.position, Quaternion.identity);
        newBallista.GetComponent<AttackerTroopsSP>().AttackButton= _attackButton;
        newBallista.GetComponent<AttackerTroopsSP>().TroopID = _troopID;
        FieldDeactivate();
        AddToList(newBallista);
    }
    private void CanonSpawn()
    {
        GameObject newCanon = Instantiate(_canon, _recruitFields[_currentFieldID].gameObject.transform.position, Quaternion.identity);
        newCanon.GetComponent<AttackerTroopsSP>().AttackButton= _attackButton;
        newCanon.GetComponent<AttackerTroopsSP>().TroopID = _troopID;
        FieldDeactivate();
        AddToList(newCanon);
    }
    private void FieldDeactivate()
    {
        Destroy(_recruitFields[_currentFieldID].gameObject);
        AttackersTroopsUISetActive(false);
    }
    private void AddToList(GameObject newTroop)
    {
        currentTroops.Add(newTroop);
        Debug.Log("Troop id: " + _troopID);
        _troopID++;
        Debug.Log(newTroop.ToString());
    }
    public static void RemoveFromList(int p_troopID)
    {
        currentTroops.RemoveAt(p_troopID); // removes obj from list of current id 
    }
    private void Start()
    {
        _batteringRamButton.onClick.AddListener(BatteringRamSpawn);
        _ballistaButton.onClick.AddListener(BallistaSpawn);
        _catapultButton.onClick.AddListener(CatapultSpawn);
        _canonButton.onClick.AddListener(CanonSpawn);
    }
}
