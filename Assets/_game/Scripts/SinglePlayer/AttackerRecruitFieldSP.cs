using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackerRecruitFieldSP : MonoBehaviour
{
    [SerializeField] private SpawnManagerSP spawnManager;

    public int fieldID = 0;

    public bool canAct = false;

    public void OnMouseDown()
    {
        if (canAct == true)
        {
            ObjClicked();
        }
    }

    public void ObjClicked()
    {
        spawnManager.PrepareToSpawn(fieldID);
    }
}
