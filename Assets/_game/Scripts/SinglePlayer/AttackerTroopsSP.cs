using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttackerTroopsSP : MonoBehaviour
{
    [SerializeField] private int _attackPower = 0;
    [SerializeField] private int _health = 0;
    [SerializeField] private Button _attackButton;
    public Button AttackButton { get { return _attackButton; } set { _attackButton = value; } }

    [SerializeField] private int _troopID;
    public int TroopID { get { return _troopID; }set { _troopID = value; } }

    public bool canAct = false;

    private void OnMouseDown()
    {
        if (canAct == true)
        {
            AttackButton.onClick.AddListener(AttackersTroopsAttacking);
            AttackerClicked();
        }
    }
    private void AttackerClicked()
    {
        AttackButton.gameObject.SetActive(true);
    }
    private void AttackersTroopsAttacking()
    {
        AttackerCameraRaycastSP.attackersTroopsAttacking = true;
        AttackerCameraRaycastSP.damageAmount = _attackPower;
        AttackButton.onClick.RemoveAllListeners();
        AttackButton.gameObject.SetActive(false);
    }
    public void GetAttacked(int p_damageAmount)
    {
        _health -= p_damageAmount;
        CheckHP();
    }
    private void CheckHP()
    {
        if (_health > 0)
        {
            return;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    // code for dying and receive damage
}