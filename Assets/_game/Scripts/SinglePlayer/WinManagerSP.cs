using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WinManagerSP : MonoBehaviour
{
    [SerializeField] private ReinforcementsManagerSP _reinforcementsManager;
    [SerializeField] private AttackerCameraRaycastSP _cameraRaycast = null;
    [SerializeField] private DefenderTroopsSP[] _defenderTroops = null;

    [SerializeField] private Button _exitMenuButton = null;

    [SerializeField] private TMP_Text _attackerWinText = null;
    [SerializeField] private TMP_Text _defenderWinText = null;
    
    private void DisableScripts() // disable the scripts 
    {
        for (int i = 0; i < _defenderTroops.Length; i++)
        {
            _defenderTroops[i].gameObject.SetActive(false);
        }
        _cameraRaycast.gameObject.SetActive(false);
    }
    public void AttackerWin()
    {
        DisableScripts();
        _reinforcementsManager.gameInPlay = false;
        _attackerWinText.gameObject.SetActive(true);
        _exitMenuButton.gameObject.SetActive(true);
    }
    public void DefenderWin()
    {
        DisableScripts();
        _defenderWinText.gameObject.SetActive(true);
        _exitMenuButton.gameObject.SetActive(true);
    }
    public void ExitToMenu()
    {
        SceneManager.LoadScene(0);
    }
    private void Start()
    {
        _exitMenuButton.onClick.AddListener(ExitToMenu);
    }
}