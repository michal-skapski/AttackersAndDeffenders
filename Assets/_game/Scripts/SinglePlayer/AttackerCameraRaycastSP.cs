using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackerCameraRaycastSP : MonoBehaviour
{
    [SerializeField] private Camera _camera = null;

    private AttackerRecruitFieldSP _attackerRecruitField = null;
    private AttackerTroopsSP _attackerTroops = null;
    private DefenderTroopsSP _defenderTroops = null;
    private SpawnManagerSP _spawnManager = null;
    private WallBehaviourSP _wallBehaviour = null;

    private Vector2 _mouseScreenPos;

    public static bool attackersTroopsAttacking = false;

    private RaycastHit _hit;
    private Ray _ray;

    public static int damageAmount = 0;

    private void Raycast()
    {
        _mouseScreenPos = Input.mousePosition;

        _ray = _camera.ScreenPointToRay(_mouseScreenPos);

        if(Physics.Raycast(_ray, out _hit) && attackersTroopsAttacking == false)
        {
            RaycastCheckerFriend();    
        }
        else if(Physics.Raycast(_ray, out _hit) && attackersTroopsAttacking == true)
        {
            RaycastCheckerEnemy();
            DefenderWallObj();
        }
    }
    private void RaycastCheckerFriend()
    {
        if (_hit.collider.gameObject.GetComponent<AttackerRecruitFieldSP>())
        {
            _attackerRecruitField = _hit.transform.GetComponent<AttackerRecruitFieldSP>();
            _attackerRecruitField.canAct = true;
        }
        else if (_hit.collider.gameObject.GetComponent<AttackerTroopsSP>())
        {
            _attackerTroops = _hit.transform.gameObject.GetComponent<AttackerTroopsSP>();
            _attackerTroops.canAct= true;
        }
        else
        {
            _attackerRecruitField.canAct = false;
            _attackerTroops.canAct = false;
        }
    }
    private void RaycastCheckerEnemy()
    {
        if(Physics.Raycast(_ray, out _hit) && attackersTroopsAttacking ==true)
        {
            AttackerTroopsAttacking();
        }
    }
    private void DefenderWallObj()
    {
        if (_hit.collider.gameObject.GetComponent<WallBehaviourSP>())
        {
            _wallBehaviour = _hit.collider.gameObject.GetComponent<WallBehaviourSP>();
            _wallBehaviour.isAttacked = true;
        }
    }

    private void AttackerTroopsAttacking()
    {
        // attack
        if (_hit.collider.gameObject.GetComponent<DefenderTroopsSP>())
        {
            DefenderTroopsSP attackedObj = _hit.collider.gameObject.GetComponent<DefenderTroopsSP>();
            attackedObj.isAttacked = true;
            attackedObj.damage = damageAmount;
        }
    }
    private void Update()
    {
        Raycast();
        Debug.Log("Attackers troops attacking: " + attackersTroopsAttacking);
        Debug.Log("Attack power: "+damageAmount);
    }
}