using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WallManagerSP : MonoBehaviour
{
    [SerializeField] private WinManagerSP _winManager = null;
    [SerializeField] private TextMeshProUGUI _wallHealthText = null;

    private const string _entryText = "Wall: ";

    public int wallHealth = 100;

    public void DamageWall(int p_damageAmount)
    {
        wallHealth -= p_damageAmount;
        AttackerCameraRaycastSP.attackersTroopsAttacking = false;
        AttackerCameraRaycastSP.damageAmount = 0;
        LifeCheck();
    }
    private void LifeCheck()
    {
        if (wallHealth <= 0)
        {
            _winManager.AttackerWin();
        }
    }
    private void Update()
    {
        _wallHealthText.text = _entryText + wallHealth; // Updating the health
    }
}