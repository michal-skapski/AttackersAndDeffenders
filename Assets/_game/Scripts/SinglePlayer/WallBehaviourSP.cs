using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallBehaviourSP : MonoBehaviour
{
    [SerializeField] private WallManagerSP _wallManager = null;

    public bool isAttacked = false;

    private void OnMouseDown()
    {
        if (isAttacked is true) 
        {
            ReceiveDamage(AttackerCameraRaycastSP.damageAmount);
        }
    }
    private void ReceiveDamage(int p_damageAmount)
    {
        _wallManager.DamageWall(p_damageAmount);
        isAttacked = false;
    }
}