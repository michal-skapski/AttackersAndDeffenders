using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;
using Fusion.Sockets;
using System;
using UnityEngine.SceneManagement;


public class BasicSpawner : MonoBehaviour, INetworkRunnerCallbacks // rpc - wysy�anie message
{
    [SerializeField] private GameObject _preparationPhase = null;
    [SerializeField] private SitePickManager _sitePickManager = null;
    [SerializeField] private NetworkPrefabRef[] _playerPrefab = null;
    private Dictionary<PlayerRef, NetworkObject> _spawnedCharacters = new Dictionary<PlayerRef, NetworkObject>();

    [SerializeField] private Vector3 _playerStartPos;

    [SerializeField] private Camera _startCamera = null;

    private int _amountOfPlayers = 0;
    private int _onePlayer = 1;
    private int _twoPlayers = 2;
    private int _defenderID = 0;
    private int _attackerID = 1;

    private NetworkObject _networkPlayerObject;

    [SerializeField] private ReinforcementsManager _reinforcementsManager = null;
    [SerializeField] private WallManager _wallManager = null;
    [SerializeField] private AttackerSpawnManager _attackerSpawnManager = null;
    [SerializeField] private DefenderSpawnManager _defenderSpawnManager = null;

    public void OnPlayerJoined(NetworkRunner runner, PlayerRef player)
    {
        if (runner.IsServer)
        {
            if(_sitePickManager.isDefender == true && _amountOfPlayers == 0)
            {
                _networkPlayerObject = runner.Spawn(_playerPrefab[_defenderID], _playerStartPos, Quaternion.identity, player);

            }
            else if (_sitePickManager.isDefender == false || _amountOfPlayers==_onePlayer)
            {
                _networkPlayerObject = runner.Spawn(_playerPrefab[_attackerID], _playerStartPos, Quaternion.identity, player);
                _reinforcementsManager.startReinforcementsTimer = true;

            }
            else
            {
                _networkPlayerObject = runner.Spawn(_playerPrefab[_defenderID], _playerStartPos, Quaternion.identity, player);
            }
            _amountOfPlayers++;
            // Create a unique position for the player
            // Keep track of the player avatars so we can remove it when they disconnect
            // here adds the adder script

            _spawnedCharacters.Add(player, _networkPlayerObject);
        }
    }

    public void OnPlayerLeft(NetworkRunner runner, PlayerRef player)
    {
        // Find and remove the players avatar
        if (_spawnedCharacters.TryGetValue(player, out NetworkObject networkObject))
        {
            runner.Despawn(networkObject);
            _spawnedCharacters.Remove(player);
        }
    }
    public void OnInput(NetworkRunner runner, NetworkInput input) { }
    public void OnInputMissing(NetworkRunner runner, PlayerRef player, NetworkInput input) { }
    public void OnShutdown(NetworkRunner runner, ShutdownReason shutdownReason) { }
    public void OnConnectedToServer(NetworkRunner runner) { }
    public void OnDisconnectedFromServer(NetworkRunner runner) { }
    public void OnConnectRequest(NetworkRunner runner, NetworkRunnerCallbackArgs.ConnectRequest request, byte[] token) { }
    public void OnConnectFailed(NetworkRunner runner, NetAddress remoteAddress, NetConnectFailedReason reason) { }
    public void OnUserSimulationMessage(NetworkRunner runner, SimulationMessagePtr message) { }
    public void OnSessionListUpdated(NetworkRunner runner, List<SessionInfo> sessionList) { }
    public void OnCustomAuthenticationResponse(NetworkRunner runner, Dictionary<string, object> data) { }
    public void OnHostMigration(NetworkRunner runner, HostMigrationToken hostMigrationToken) { }
    public void OnReliableDataReceived(NetworkRunner runner, PlayerRef player, ArraySegment<byte> data) { }
    public void OnSceneLoadDone(NetworkRunner runner) { }
    public void OnSceneLoadStart(NetworkRunner runner) { }

    private NetworkRunner _runner;
    async void StartGame(GameMode mode)
    {
        // Create the Fusion runner and let it know that we will be providing user input
        _runner = gameObject.AddComponent<NetworkRunner>();
        _reinforcementsManager.runner = _runner;
        _wallManager.runner = _runner;
        _attackerSpawnManager.runner = _runner; // for rpc
        _defenderSpawnManager.runner = _runner;
        _runner.ProvideInput = true;
        DestroyCamera();
        _preparationPhase.gameObject.SetActive(true); // starting preparation time
        // Start or join (depends on gamemode) a session with a specific name
        await _runner.StartGame(new StartGameArgs()
        {
            GameMode = mode,
            SessionName = "TestRoom",
            Scene = SceneManager.GetActiveScene().buildIndex,
            
            SceneManager = gameObject.AddComponent<NetworkSceneManagerDefault>()
        });
    }
    private void DestroyCamera()
    {
        Destroy(_startCamera.gameObject); // disabling start camera
    }
    public void OnLaunchDemand(bool p_isHost)
    {
        if(p_isHost is true)
        {
            StartGame(GameMode.Host);
        }
        else if(p_isHost is false)
        {
            StartGame(GameMode.Client);
        }
    }
    private void OnGUI()
    {
        if (_runner == null)
        {
            /*
            if (GUI.Button(new Rect(0, 0, 200, 40), "Host"))
            {
                StartGame(GameMode.Host);
            }
            if (GUI.Button(new Rect(0, 40, 200, 40), "Join"))
            {
                StartGame(GameMode.Client);
            }
            */
        }
    }

}