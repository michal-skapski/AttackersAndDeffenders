using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SitePickManager : MonoBehaviour
{
    private const float _halfChance = 0.5f;
    public bool isDefender;
    public void IsDefender()
    {
        if (Random.value >= _halfChance)
        {
            isDefender = true;
        }
        else
        {
            isDefender = false;
        }
    }
    private void Start()
    {
        //IsDefender();
        isDefender = true; // for test
    }
}