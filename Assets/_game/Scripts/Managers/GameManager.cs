using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private int _fpsTarget = 144;

    private void Awake()
    {
        Application.targetFrameRate = _fpsTarget;
    }
}