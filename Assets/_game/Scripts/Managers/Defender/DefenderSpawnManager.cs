using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fusion;

public class DefenderSpawnManager : NetworkBehaviour
{

    [SerializeField] public GameObject[] _recruitFields = null;

    [SerializeField] private Button _attackButton = null;

    [SerializeField] private Button _ballistaButton = null;
    [SerializeField] private Button _catapultButton = null;
    [SerializeField] private Button _archersButton = null;

    [SerializeField] private GameObject _ballista = null;
    [SerializeField] private GameObject _catapult = null;
    [SerializeField] private GameObject _archers = null;


    [SerializeField] private Button[] _troopsButtons = null;

    [SerializeField] private GameUI _gameUI = null;


    private int _currentFieldID = 0;

    public NetworkRunner runner;
    [SerializeField] private BasicSpawner _basicSpawner;

    public void DefenderTroopsUISetActive(bool p_setActive)
    {
        for (int i = 0; i < _troopsButtons.Length; i++)
        {
            _troopsButtons[i].gameObject.SetActive(p_setActive);
        }

        if(p_setActive)
        {
           // _gameUI.ShowGUI();
        }
        else
        {
            //_gameUI.HideGUI();
        }
    }
    public void PrepareToSpawn(int p_fieldID)
    {
        _currentFieldID = p_fieldID;
        DefenderTroopsUISetActive(true); // enabling buttons
        Debug.Log("Defender current troops id field: " + _currentFieldID);
    }
    private void BallistaSpawn()
    {
        //GameObject newBallista = Instantiate(_ballista, _recruitFields[_currentFieldID].gameObject.transform.position, Quaternion.identity);
        //newBallista.GetComponent<DefenderTroops>().AttackButton = _attackButton;
        Rpc_SpawnObjBallista();
        Rpc_FieldDeactivate();
    }
    [Rpc] 
    private void Rpc_SpawnObjBallista()
    {
        NetworkObject newBallista = runner.Spawn(_ballista, _recruitFields[_currentFieldID].gameObject.transform.position, Quaternion.identity);
        newBallista.GetComponent<DefenderTroops>().AttackButton = _attackButton;
    }
    private void CatapultSpawn()
    {
        //GameObject newCatapult = Instantiate(_catapult, _recruitFields[_currentFieldID].gameObject.transform.position, Quaternion.identity);
        //newCatapult.GetComponent<DefenderTroops>().AttackButton = _attackButton;
        Rpc_CatapultSpawn();
        Rpc_FieldDeactivate();
    }
    [Rpc]
    private void Rpc_CatapultSpawn()
    {
        NetworkObject newCatapult = runner.Spawn(_catapult, _recruitFields[_currentFieldID].gameObject.transform.position, Quaternion.identity);
        newCatapult.GetComponent<DefenderTroops>().AttackButton = _attackButton;
    }
    private void ArchersSpawn()
    {
        //GameObject newArchers = Instantiate(_archers, _recruitFields[_currentFieldID].gameObject.transform.position, Quaternion.identity);
        //newArchers.GetComponent<DefenderTroops>().AttackButton = _attackButton;        
        Rpc_ArchersSpawn();
        Rpc_FieldDeactivate();
    }
    [Rpc]
    private void Rpc_ArchersSpawn()
    {
        NetworkObject newArchers = runner.Spawn(_archers, _recruitFields[_currentFieldID].gameObject.transform.position, Quaternion.identity);
        newArchers.GetComponent<DefenderTroops>().AttackButton = _attackButton;
    }
    [Rpc]
    private void Rpc_FieldDeactivate()
    {
        //_recruitFields[_currentFieldID].gameObject.SetActive(false); // add code for disabled fields to reactivate later 
        Destroy(_recruitFields[_currentFieldID].gameObject);
        DefenderTroopsUISetActive(false);
    }
    private void Start()
    {
        _ballistaButton.onClick.AddListener(BallistaSpawn);
        _catapultButton.onClick.AddListener(CatapultSpawn);
        _archersButton.onClick.AddListener(ArchersSpawn);
    }
}