using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fusion;


public class AttackerSpawnManager : NetworkBehaviour
{
    [SerializeField] private GameObject[] _recruitFields = null;

    [SerializeField] private Button _attackButton = null;

    [SerializeField] private Button _batteringRamButton = null;
    [SerializeField] private Button _ballistaButton = null;
    [SerializeField] private Button _catapultButton = null;
    [SerializeField] private Button _archersButton = null;

    [SerializeField] private GameObject _batteringRam = null;
    [SerializeField] private GameObject _ballista = null;
    [SerializeField] private GameObject _catapult = null;
    [SerializeField] private GameObject _archers = null;

    [SerializeField] private Button[] _troopsButtons = null;

    [SerializeField] private GameUI _gameUI = null;

    private int _currentFieldID = 0;

    [SerializeField] public NetworkRunner runner;
    [SerializeField] private BasicSpawner _basicSpawner;



    public void AttackerTroopUISetActive(bool p_setActive)
    {
        for (int i = 0; i < _troopsButtons.Length; i++)
        {
            _troopsButtons[i].gameObject.SetActive(p_setActive);
        }
        /*
        if (p_setActive)
        {
            _gameUI.ShowGUI();
        }
        else
        {
            _gameUI.HideGUI();
        }
        */
    }
    public void PrepareToSpawn(int p_fieldID)
    {
        _currentFieldID = p_fieldID;
        AttackerTroopUISetActive(true);
        Debug.Log("Attacker current  troops id field: " + _currentFieldID);
    }
    private void BatteringRamSpawn()
    {
        //GameObject newBatteringRam = Instantiate(_batteringRam, _recruitFields[_currentFieldID].gameObject.transform.position, Quaternion.identity);
        //newBatteringRam.GetComponent<BallistaAttacker>().AttackButton = _attackButton;
        Debug.Log("Spawning ram");
        Rpc_SpawnObjBatteringRam();
        Rpc_FieldDeactivate();
    }
    [Rpc]
    private void Rpc_SpawnObjBatteringRam()
    {
        NetworkObject newBatteringRam = runner.Spawn(_batteringRam, _recruitFields[_currentFieldID].gameObject.transform.position, Quaternion.identity);
        newBatteringRam.GetComponent<AttackerTroops>().AttackButton = _attackButton;
        Debug.Log("Spawning ram rpc");
        //GameObject newBatteringRam = Instantiate(_batteringRam, _recruitFields[_currentFieldID].gameObject.transform.position, Quaternion.identity);
        //newBatteringRam.GetComponent<BatteringRamAttacker>().AttackButton = _attackButton;
    }
    private void BallistaSpawn()
    {
        Rpc_BallistaSpawn();
        Rpc_FieldDeactivate();
    }
    [Rpc]
    private void Rpc_BallistaSpawn()
    {
        NetworkObject newBallista = runner.Spawn(_ballista, _recruitFields[_currentFieldID].gameObject.transform.position, Quaternion.identity);
        newBallista.GetComponent<AttackerTroops>().AttackButton = _attackButton;
    }
    private void CatapultSpawn()
    {
        Rpc_SpawnCatapult();
        Rpc_FieldDeactivate();
    }
    [Rpc]
    private void Rpc_SpawnCatapult()
    {
        NetworkObject newCatapult = runner.Spawn(_catapult, _recruitFields[_currentFieldID].gameObject.transform.position, Quaternion.identity);
        newCatapult.GetComponent<AttackerTroops>().AttackButton = _attackButton;
    }
    private void ArchersSpawn()
    {
        //GameObject newArchers = Instantiate(_archers, _recruitFields[_currentFieldID].gameObject.transform.position, Quaternion.identity);
        //newArchers.GetComponent<ArchersAttacker>().AttackButton = _attackButton; // fabrication
        Rpc_SpawnObjArchers();
        Rpc_FieldDeactivate();
    }
    [Rpc]
    private void Rpc_SpawnObjArchers()
    {
        NetworkObject newArchers = runner.Spawn(_archers, _recruitFields[_currentFieldID].gameObject.transform.position, Quaternion.identity);
        newArchers.GetComponent<AttackerTroops>().AttackButton = _attackButton;
    }
    [Rpc]
    private void Rpc_FieldDeactivate() // field obj deactivate
    {
        // add code for disabled fields to reactivate later 
        //_recruitFields[_currentFieldID].gameObject.SetActive(false); // disabling the spawn field
        Destroy(_recruitFields[_currentFieldID].gameObject);
        AttackerTroopUISetActive(false); // disabling the ui
    }
    private void Start()
    {
        _batteringRamButton.onClick.AddListener(BatteringRamSpawn);
        _ballistaButton.onClick.AddListener(BallistaSpawn);
        _catapultButton.onClick.AddListener(CatapultSpawn);
        _archersButton.onClick.AddListener(ArchersSpawn);
    }
}