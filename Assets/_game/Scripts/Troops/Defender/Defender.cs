using Fusion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defender : MonoBehaviour
{
    [SerializeField] private int _lives = 0;

    public bool isAttacked = false;
    public int damage;

    private void OnMouseDown()
    {
        if(isAttacked == true)
        {
            Damage(damage);
        }
    }
    public void Damage(int p_damageAmount)
    {
        _lives -= p_damageAmount;
        Debug.Log("Defender takes damage");
        AttackerCameraRaycast.attackersTroopsAttacking = false;
        LifeCheck();
        isAttacked = false;
    }
    [Rpc]
    private void LifeCheck()
    {
        if (_lives <= 0)
        {
            Destroy(gameObject);
        }
    }
}
