using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DefenderTroops : MonoBehaviour
{
    [SerializeField] private int _attackPower = 0; 
    [SerializeField] private Button _attackButton;
    
    public Button AttackButton { get { return _attackButton; } set { _attackButton = value; } }

    public bool clickedByFriend = false;
    public bool canAct = false;

    private void OnMouseDown()
    {
        if (canAct == true)
        {
            AttackButton.onClick.AddListener(DefenderTroopsAttacking);
            DefenderClicked();
        }
    }
    private void DefenderClicked()
    {
        AttackButton.gameObject.SetActive(true);        
    }
    private void DefenderTroopsAttacking()
    {
        DefenderCameraRaycast.defenderTroopsAttacking = true;
        DefenderCameraRaycast.damageAmount = _attackPower;
        AttackButton.onClick.RemoveAllListeners();
        AttackButton.gameObject.SetActive(false);
    }
}
