using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrebuchetAttacker : MonoBehaviour
{
    private Button _attackButton = null;
    public Button AttackButton { get { return _attackButton; } set { _attackButton = value; } }
}