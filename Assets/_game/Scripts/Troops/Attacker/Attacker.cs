using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;

public class Attacker : MonoBehaviour
{
    [SerializeField] private int _lives = 0; // declare the health for each obj
    public bool isAttacked = false;
    public int damage;
    private void OnMouseDown()
    {
        if(isAttacked == true)
        {
            Damage(damage);
        }
    }
    public void Damage(int p_damageAmount)
    {
        _lives -= p_damageAmount;
        Debug.Log("Take damage");
        DefenderCameraRaycast.defenderTroopsAttacking = false;
        LifeCheck();
        isAttacked = false; // disable the attack
    }
    private void LifeCheck()
    {
        if (_lives <= 0)
        {
            Rpc_Death();
        }
    }
    [Rpc]
    private void Rpc_Death()
    {
        Destroy(gameObject);
    }
}
