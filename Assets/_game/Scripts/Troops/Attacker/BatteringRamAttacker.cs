using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BatteringRamAttacker : MonoBehaviour
{
    [SerializeField] private Button _attackButton = null;
    [SerializeField] private int _attackPower = 0; // enhance later

    public Button AttackButton { get { return _attackButton; } set { _attackButton = value; } } // fabrication
    public bool clickedByFriend = false;
    public bool canAct = false;

    private void OnMouseDown()
    {
        if (canAct == true)
        {
            AttackingBatteringRamClicked();
        }

    }
    public void AttackingBatteringRamClicked()
    {
        AttackButton.gameObject.SetActive(true);
    }
    private void BatteringRamMove()
    {
        // move code later
        AttackButton.onClick.RemoveAllListeners(); // removing listeners just in case
        AttackButton.gameObject.SetActive(false); // disabling obj
    }
    private void Start()
    {
        AttackButton.onClick.AddListener(BatteringRamMove);
    }
}
