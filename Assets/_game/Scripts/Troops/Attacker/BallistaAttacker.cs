using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BallistaAttacker : MonoBehaviour
{
    [SerializeField] private Button _attackButton = null;
    [SerializeField] private int _attackPower = 0; // enhance later

    public Button AttackButton { get { return _attackButton; } set { _attackButton = value; } } // fabrication
    public bool clickedByFriend = false;
    public bool canAct = false;

    private void OnMouseDown()
    {
        if (canAct == true)
        {
            AttackingBallistaClicked();
            AttackButton.onClick.AddListener(BallistaAttacking);
        }
    }
    private void AttackingBallistaClicked()
    {
        AttackButton.gameObject.SetActive(true);
    }
    private void BallistaAttacking()
    {
        AttackerCameraRaycast.attackersTroopsAttacking = true;
        AttackerCameraRaycast.damageAmount = _attackPower;
        AttackButton.onClick.RemoveAllListeners();
        AttackButton.gameObject.SetActive(false);

    }
}
