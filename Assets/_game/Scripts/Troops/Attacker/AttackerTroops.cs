using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttackerTroops : MonoBehaviour
{
    [SerializeField] private int _attackPower = 0;

    [SerializeField] private Button _attackButton = null;

    public Button AttackButton { get { return _attackButton; } set { _attackButton = value; } }

    public bool clickedByFriend = false;
    public bool canAct = false;

    private void OnMouseDown()
    {
        if (canAct == true)
        {
            AttackButton.onClick.AddListener(AttackerTroopsAttacking);
            AttackerClicked();
        }
    }

    private void AttackerClicked()
    {
        AttackButton.gameObject.SetActive(true);
    }
    private void AttackerTroopsAttacking()
    {
        AttackerCameraRaycast.attackersTroopsAttacking = true;
        AttackerCameraRaycast.damageAmount = _attackPower;
        Debug.Log("Attackers troops attacking: " + AttackerCameraRaycast.attackersTroopsAttacking);
        AttackButton.onClick.RemoveAllListeners(); // removing listeners just in case
        AttackButton.gameObject.SetActive(false); // disabling the button
        //AttackerCameraRaycast.attackersTroopsAttacking = false; // disabling bool for build purposes
    }
}