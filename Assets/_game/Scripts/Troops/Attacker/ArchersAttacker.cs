using Fusion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArchersAttacker : NetworkBehaviour
{
    [SerializeField] private Button _attackButton = null;
    [SerializeField] private int _attackPower = 0; // enhance later

    public Button AttackButton { get { return _attackButton; } set { _attackButton = value; } } // fabrication
    public bool clickedByFriend = false;
    public bool canAct = false;
    private void OnMouseDown()
    {
        if (canAct == true)
        {
            AttackingArchersClicked();
            AttackButton.onClick.AddListener(ArchersAttacking);
        }
    }
    private void AttackingArchersClicked()
    {
        AttackButton.gameObject.SetActive(true);
    }
    private void ArchersAttacking()
    {
        AttackerCameraRaycast.attackersTroopsAttacking = true;
        AttackerCameraRaycast.damageAmount = _attackPower;
        AttackButton.onClick.RemoveAllListeners(); // removing listeners just in case
        AttackButton.gameObject.SetActive(false); // disabling obj
    }
}