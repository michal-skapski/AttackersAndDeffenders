using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField] private BasicSpawner _spawner = null;
    [SerializeField] private Button _hostButton = null;
    [SerializeField] private Button _clientButton = null;

    [SerializeField] private GameObject _attackerPausePanel = null;
    [SerializeField] private GameObject _defenderPausePanel = null;
    private void HostButton()
    {
        _spawner.OnLaunchDemand(true);
        _hostButton.gameObject.SetActive(false);
        _clientButton.gameObject.SetActive(false);
    }
    private void ClientButton()
    {
        _spawner.OnLaunchDemand(false);
        _hostButton?.gameObject.SetActive(false);
        _clientButton.gameObject.SetActive(false);
    }

    public void AttackerPauseButton()
    {
        _attackerPausePanel.SetActive(true);
    }
    public void DefenderPauseButton()
    {
        _defenderPausePanel.SetActive(true);
    }

    public void AttackerReturnButton()
    {
        _attackerPausePanel.SetActive(false);
    }
    public void DefenderReturnButton()
    {
        _defenderPausePanel.SetActive(false);
    }

    public void AttackerQuitButton()
    {
        //wyjscie z gry + walkower
        Debug.Log("Quit button pressed");
    }
    public void DefenderQuitButton()
    {
        //wyjscie z gry + walkower
        Debug.Log("Quit button pressed");
    }

    private void Start()
    {
        _hostButton.onClick.AddListener(HostButton);
        _clientButton.onClick.AddListener(ClientButton);
    }
}