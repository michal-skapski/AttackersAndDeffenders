using Fusion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ReinforcementsManager : NetworkBehaviour
{
    [SerializeField] private TextMeshProUGUI _reinforcementsText = null;
    [SerializeField] public TextMeshProUGUI winText;
    [SerializeField] private Button _winButton = null;
    public NetworkRunner runner = null;

    [Networked] public string entryText { get; set; } = "Reinforcements arrive in arround 2 minutes";
    private string _oneMinText = "Reinforcements arrive in: ";
    [Networked] public int timeLeft { get; set; } = 30;

    private int _oneMin = 60;
    private int _oneSec = 1;
    private string _emptyText = "";

    [Networked] public bool startReinforcementsTimer { get; set; } = false;
    [Rpc]
    private void Rpc_TextUpdate()
    {
        if (timeLeft >= _oneMin)
        {
            entryText = _oneMinText + timeLeft;
        }
        else if(timeLeft == 0)
        {
            entryText = _emptyText;
            _winButton.gameObject.SetActive(true);
            
        }
    }
    public IEnumerator ReinforcementsTimer()
    {
        timeLeft--;
        yield return new WaitForSeconds(_oneSec);
    }
    public override void FixedUpdateNetwork()
    {
        Rpc_TextUpdate();
        if (startReinforcementsTimer == true)
        {
            StartCoroutine(ReinforcementsTimer());
        }
    }
    private void WinButton()
    {
        SceneManager.LoadScene(0);
    }
    private void Start()
    {
        _winButton.onClick.AddListener(WinButton);
    }
}