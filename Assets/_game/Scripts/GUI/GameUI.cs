using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour
{
    [SerializeField] private RectTransform _ui = null;
    [SerializeField] private int _showCoords = 0;
    [SerializeField] private int _hideCoords = 0;
    private bool _visible = false;
    public void ShowGUI()
    {
        _ui.anchoredPosition = new Vector2(0, _showCoords);
        _visible = true;
    }

    public void HideGUI()
    {
        _ui.anchoredPosition = new Vector2(0, _hideCoords);
        _visible = false;
    }

    /*private void Update()
    {
        if(Input.GetKeyDown(KeyCode.U))
        {
            if(_visible)
            {
                HideGUI();
            }
            else
            {
                ShowGUI();
            }
        }
    }*/
}
