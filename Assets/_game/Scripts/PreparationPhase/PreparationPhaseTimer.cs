using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PreparationPhaseTimer : MonoBehaviour
{
    [SerializeField] private PhaseManager _phaseManager = null;
    [SerializeField] TMP_Text text;

    [SerializeField] private int _timeLeft = 0;
    private float _oneSec = 1f;
    private bool _takeTime = false;

    private string _preparationText = "Preparation Phase Ending In: ";
    private void TimeCheck()
    {
        if (_timeLeft == 0)
        {
            _phaseManager.PreparationTimeDone();
            text.gameObject.SetActive(false); // disabling the text for ui puproses
            this.gameObject.SetActive(false); // disabling the timer for optymalization purposes
        }
    }
    public IEnumerator TimerTake()
    {
        _takeTime = true;
        yield return new WaitForSeconds(_oneSec);
        _timeLeft--;
        text.text = _preparationText + _timeLeft;
        TimeCheck();
        _takeTime = false; // needs to put bool here for update to not starting it thousands of times
    }
    private void Start()
    {
        text.text = _preparationText + _timeLeft;
    }
    private void Update()
    {
        if (_timeLeft > 0 && _takeTime == false)
        {
           StartCoroutine(TimerTake());
        }
    }
}