using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackerCameraRaycast : MonoBehaviour
{
    [SerializeField] private Camera _camera = null;

    private AttackerRecruitField _attackerRecruitObj = null;
    private AttackerSiegeCamp _attackerSiegeCamp = null;
    private DefenderRecruitField _defenderRecruitField = null;
    private WallBehaviour _wallBehaviour = null;
    
    private ArchersAttacker _attackerArcherObj = null;
    private BallistaAttacker _attackerBallistaObj = null;
    private BatteringRamAttacker _attackerBatteringRamObj = null;
    private CatapultAttacker _attackerCatapultObj = null;
    private MilkaAttacker _attackerMilkaObj = null;
    private TrebuchetAttacker _attackerTrebuchetObj = null;


    private Vector2 _mouseScreenPos;
    public static bool attackersTroopsAttacking = false;
    private RaycastHit _hit;
    private Ray _ray;

    public static int damageAmount = 0;

    private AttackerTroops _attackerTroops = null;

    private void Raycast()
    {
        _mouseScreenPos = Input.mousePosition;

        _ray = _camera.ScreenPointToRay(_mouseScreenPos);

        if (Physics.Raycast(_ray, out _hit) && attackersTroopsAttacking == false)
        {
            RaycastCheckerFriend();
        }
        else if (Physics.Raycast(_ray, out _hit) && attackersTroopsAttacking == true) // methods for damage
        {
            DefenderWallObj();
            RaycastCheckerEnemy();
        }
    }
    private void RaycastCheckerFriend() // code for friend building
    {
        if (_hit.collider.gameObject.GetComponent<AttackerRecruitField>())
        {
            _attackerRecruitObj = _hit.transform.GetComponent<AttackerRecruitField>();
            _attackerRecruitObj.canAct = true;
            _attackerRecruitObj.clickedByFriend = true;
        }
        else if (_hit.collider.gameObject.GetComponent<AttackerTroops>())
        {
            _attackerTroops = _hit.transform.GetComponent<AttackerTroops>();
            _attackerTroops.canAct = true;
            Debug.Log("Attacker pointing");
            AttackerTroopsAttacking();
        }
        else
        {
            //_attackerRecruitObj.canAct = false;
            //_attackerRecruitObj.clickedByFriend = false;
        }
    }
    
    private void RaycastCheckerEnemy()
    {
        if(Physics.Raycast(_ray, out _hit) && attackersTroopsAttacking == true)
        {
            AttackerTroopsAttacking();
        }
    }
    /*
    private void AttackerBuildObj()
    {
        if (_attackerRecruitObj != null)
        {
            _attackerRecruitObj.clickedByFriend = true;
            _attackerRecruitObj.canAct = true;
            Debug.Log("Pointing attacker obj");
        }
    }
    */
    
    private void DefenderWallObj()
    {
        if (_hit.collider.gameObject.GetComponent<WallBehaviour>())
        {
            _wallBehaviour = _hit.collider.gameObject.GetComponent<WallBehaviour>();
            Debug.Log("Pointing wall");
            AttackDefenderWall();
        }
    }
    private void AttackerTroopsAttacking()
    {
        if (_hit.collider.gameObject.GetComponent<Defender>())
        {
            Defender attackedObj = _hit.collider.gameObject.GetComponent<Defender>();
            Debug.Log("Attacker troops attacking");
            attackedObj.isAttacked = true;
            attackedObj.damage = damageAmount;
        }
    }
    private void AttackDefenderWall()
    {
        if (_wallBehaviour != null)
        {
            _wallBehaviour.isAttacked = true;
        }
    }
    private void Update()
    {
        Raycast();
    }
}
