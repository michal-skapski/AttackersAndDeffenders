using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderCameraRaycast : MonoBehaviour
{
    [SerializeField] private Camera _camera = null;
    private Vector2 _mouseScreenPos;


    private DefenderRecruitField _defenderRecruitField = null;


    private DefenderTroops _defenderTroops = null;

    private RaycastHit _hit;
    private Ray _ray;

    public static bool defenderTroopsAttacking = false;
    public static int damageAmount = 0;
    private void Raycast()
    {
        _mouseScreenPos = Input.mousePosition;

        _ray = _camera.ScreenPointToRay(_mouseScreenPos);
        // raycast do
        if(Physics.Raycast(_ray, out _hit) && defenderTroopsAttacking == false)
        {
            RaycastCheckerFriend();
        }
        else if(Physics.Raycast(_ray, out _hit) && defenderTroopsAttacking == true)
        {
            RaycastCheckerEnemy();
        }
    }
    private void RaycastCheckerFriend()
    {
        if (_hit.collider.gameObject.GetComponent<DefenderRecruitField>())
        {
            _defenderRecruitField = _hit.transform.GetComponent<DefenderRecruitField>();
            DefenderBuildObj();
        }
        else if (_hit.collider.gameObject.GetComponent<DefenderTroops>())
        {
            _defenderTroops = _hit.transform.GetComponent<DefenderTroops>();
            _defenderTroops.canAct = true;
            DefenderTroopsAttacking();
        }
    }
    private void RaycastCheckerEnemy()
    {
        if (Physics.Raycast(_ray, out _hit) && defenderTroopsAttacking == true)
        {
            DefenderTroopsAttacking();
        }
    }
    private void DefenderBuildObj()
    {
        if (_defenderRecruitField != null)
        {
            _defenderRecruitField.canAct = true;
            _defenderRecruitField.clickedByFriend = true;
        }
        else
        {
            _defenderRecruitField.canAct = false;
            _defenderRecruitField.clickedByFriend= false;
        }
    }
    private void DefenderTroopsAttacking()
    {
        if (_hit.collider.gameObject.GetComponent<Attacker>())
        {
            Attacker attackingObj = _hit.collider.gameObject.GetComponent<Attacker>();
            Debug.Log("Defender troops attacking");
            attackingObj.isAttacked = true;
            attackingObj.damage = damageAmount;
        }
    }
    private void Update()
    {
        Raycast();
    }
}