using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttackerRecruitField : MonoBehaviour
{
    [SerializeField] private AttackerSpawnManager _attackerSpawnManager = null;


    public int fieldID = 0;


    public bool clickedByFriend = true;
    public bool canAct = true;
    public void OnMouseDown()
    {
        if (canAct == true)
        {
            ObjClicked(clickedByFriend);
        }
    }
    public void ObjClicked(bool p_isfriend)
    {
        Debug.Log("Attacker UI obj will show");
        if (p_isfriend == true)
        {
            _attackerSpawnManager.PrepareToSpawn(fieldID);  // display the ui 
        }

    }
}