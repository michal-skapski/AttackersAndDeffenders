using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackerSiegeCamp : MonoBehaviour
{
    private static int _siegeCampHealth = 100;
    public bool clickedByFriend = false;
    public bool canAct = false;
    public static void SiegeCampDamage(int p_damageAmount)
    {
        _siegeCampHealth -= p_damageAmount;
    }
    private void OnMouseDown() // needs to add an parameter or an if statement which player clicked the object
    {
        Debug.Log("You've clicked the siege camp");
    }
    private void Update()
    {
        // code for progress bar here as well
    }
}
