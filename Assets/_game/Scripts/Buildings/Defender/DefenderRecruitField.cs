using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DefenderRecruitField : MonoBehaviour
{
    [SerializeField] private DefenderSpawnManager _defenderSpawnManager = null;
    public bool clickedByFriend = false;
    public bool canAct = false;

    public int fieldID = 0;

    private void OnMouseDown()
    {
        if (canAct == true)
        {
            ShowUI(clickedByFriend);
        }
    }
    public void ShowUI(bool p_isFriend)
    {
        Debug.Log("Defender build obj clicked");
        if(p_isFriend== true)
        {
            _defenderSpawnManager.PrepareToSpawn(fieldID);
        }
    }
}