using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EventDisplay : MonoBehaviour
{
    [SerializeField] private EventWindow _eventWindowPrefab = null;
    [SerializeField] private List<Events> _eventList = new List<Events>();

    [SerializeField] private float _minDisplayTime = 0;
    [SerializeField] private float _maxDisplayTime = 1;

    private void Start()
    {
        StartCoroutine(WaitToDisplay(Random.Range(_minDisplayTime, _maxDisplayTime), Random.Range(0, _eventList.Count)));
    }

    private void Update()
    {
        //Testing
        if(Input.GetKeyDown(KeyCode.E))
        {
            if (_eventList[0] != null)
            {
                Display(_eventList[0]);
            }
            else
            {
                Debug.Log("No event found");
            }
        }
    }

    private void Display(Events events)
    {
        _eventWindowPrefab.SetEvent(events.title, events.description, events.yesButtonText, events.noButtonText, events);
        _eventWindowPrefab.gameObject.SetActive(true);
        Debug.Log("Event displayed");
        //events.Effect();
    }

    IEnumerator WaitToDisplay(float waitTime, int eventID)
    {
        Debug.Log("Wait time: " + waitTime);
        yield return new WaitForSeconds(waitTime);
        Debug.Log("Coroutine ended");
        Display(_eventList[eventID]);
    }
}
