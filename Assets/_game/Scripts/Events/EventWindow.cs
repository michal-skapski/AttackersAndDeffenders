using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EventWindow : MonoBehaviour
{
    [SerializeField] private TMP_Text _titleText;
    [SerializeField] private TMP_Text _descriptionText;
    [SerializeField] private TMP_Text _yesButtonText;
    [SerializeField] private TMP_Text _noButtonText;

    private Events _event = null;

    public void SetEvent(string title, string desc, string yes, string no, Events events)
    {
        _titleText.text = title;
        _descriptionText.text = desc;
        _yesButtonText.text = yes;
        _noButtonText.text = no;

        _event = events;

        Debug.Log("Event text set");
    }

    public void YesButtonPress()
    {
        if(_event != null)
        {
            _event.Effect();
        }       
        gameObject.SetActive(false);
    }

    public void NoButtonPress()
    {
        gameObject.SetActive(false);
    }
}
