using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Events : MonoBehaviour
{
    public string title;
    public string description;
    public string yesButtonText;
    public string noButtonText;

    
    public virtual void Effect()
    {
        Debug.Log("Parent Class Event Effect");
    }
}
