using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Button _startButton = null;
    [SerializeField] private Button _creditsButton = null;
    [SerializeField] private Button _quitButton = null;
    [SerializeField] private Button _singlePlayerButton = null;
    [SerializeField] private Button _backButton = null;


    private const int _gameSceneID = 1;
    private const int _demoScene = 2;

    [SerializeField] private GameObject[] _creditsTXT = null;

    private void MainButtonsSetActive(bool p_setActive)
    {
        _startButton.gameObject.SetActive(p_setActive);
        _creditsButton.gameObject.SetActive(p_setActive);
        _quitButton.gameObject.SetActive(p_setActive);
        _singlePlayerButton.gameObject.SetActive(p_setActive);
        BackButtonSetActive(false);
    }
    private void SecondButtonsSetActive(bool p_setActive)
    {
        _backButton.gameObject.SetActive(p_setActive);
    }
    private void BackButtonSetActive(bool p_setActive)
    {
        _backButton.gameObject.SetActive(p_setActive);
    }
    private void SinglePlayerButton()
    {
        SceneManager.LoadScene(_demoScene);
    }
    private void StartButton()
    {
        SceneManager.LoadScene(_gameSceneID);
    }
    private void CreditsButton()
    {
        CreditsTXTSetActive(true);
        MainButtonsSetActive(false);
        BackButtonSetActive(true);        
    }
    private void CreditsTXTSetActive(bool p_setActive)
    {
        for (int i = 0; i < _creditsTXT.Length; i++)
        {
            print("Text should be on");
            _creditsTXT[i].gameObject.SetActive(p_setActive);
        }
    }
    private void QuitButton()
    {
        Application.Quit();
    }
    private void BackButton()
    {
        MainButtonsSetActive(true);
        SecondButtonsSetActive(false);
        BackButtonSetActive(false);
        CreditsTXTSetActive(false);
    }
    private void Start()
    {
        _startButton.onClick.AddListener(StartButton);
        _singlePlayerButton.onClick.AddListener(SinglePlayerButton);
        _creditsButton.onClick.AddListener(CreditsButton);
        
        _backButton.onClick.AddListener(BackButton);

        _quitButton.onClick.AddListener(QuitButton);
    }
}