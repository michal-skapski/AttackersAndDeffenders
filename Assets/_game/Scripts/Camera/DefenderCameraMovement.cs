using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderCameraMovement : MonoBehaviour
{
    [SerializeField] private float _cameraSpeed = 0f;
    [SerializeField] private float _scrollSpeed = 0f;
    [SerializeField] private float _panBorderThickness = 0f;
    [SerializeField] private Vector3 _cameraMinPosLimit;
    [SerializeField] private Vector3 _cameraMaxPosLimit;

    private const float _minScrollInput = -0.1f;
    private const float _maxScrollInput = 0.1f;

    private float _scrollInput;
    private float _horizontalInput;

    private const string _scrollAxisName = "Mouse ScrollWheel";
    private const string _mouseXAxisName = "Mouse X";

    private Vector3 _cameraPos;

    private int _middleMouseButtonID = 2;
    private bool _canMove; // a bool which make sure that we doesn't change position while rotating
    private void MoveCamera()
    {
        _cameraPos = transform.position;

        if (Input.mousePosition.y >= Screen.height - _panBorderThickness && _canMove == true)
        {
            _cameraPos.x -= _cameraSpeed * Time.deltaTime; 
        }
        else if (Input.mousePosition.y <= _panBorderThickness && _canMove ==true)
        {
            _cameraPos.x += _cameraSpeed * Time.deltaTime;
        }
        else if (Input.mousePosition.x >= Screen.width - _panBorderThickness && _canMove == true)
        {
            _cameraPos.z += _cameraSpeed * Time.deltaTime;
        }
        else if(Input.mousePosition.x <= _panBorderThickness && _canMove == true)
        {
            _cameraPos.z -= _cameraSpeed * Time.deltaTime;
        }
        CameraPosLimit();
        CameraZoom();
        transform.position = _cameraPos;
    }
    private void CameraRotation()
    {
        _horizontalInput = Input.GetAxis(_mouseXAxisName);
        if (!Input.GetMouseButton(_middleMouseButtonID))
        {
            _canMove= true;
            return;
        }
        else if(Input.GetMouseButton(_middleMouseButtonID)) 
        {
            Vector3 rotationDirection = new Vector3(0, _horizontalInput, 0);
            _canMove = false;
            transform.Rotate(rotationDirection);
        }
    }
    private void CameraZoom()
    {
        _scrollInput = Input.GetAxis(_scrollAxisName);
        if (_cameraPos.y == _cameraMaxPosLimit.y && _scrollInput == _minScrollInput)
        {
            return;
        }
        else if (_cameraPos.y == _cameraMinPosLimit.y && _scrollInput == _maxScrollInput)
        {
            return;
        }
        else
        {
            _cameraPos.y -= _scrollInput * _scrollSpeed * Time.deltaTime;
        }
    }
    private void CameraPosLimit() // clamping the positions
    {
        _cameraPos.x = Mathf.Clamp(_cameraPos.x, _cameraMinPosLimit.x, _cameraMaxPosLimit.x);
        _cameraPos.y = Mathf.Clamp(_cameraPos.y, _cameraMinPosLimit.y, _cameraMaxPosLimit.y);
        _cameraPos.z = Mathf.Clamp(_cameraPos.z, _cameraMinPosLimit.z, _cameraMaxPosLimit.z);
    }
    private void Update()
    {
        MoveCamera();
        CameraRotation();
    }
}